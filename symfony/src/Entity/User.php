<?php
	
	namespace App\Entity;
                              	
                              	use App\Repository\UserRepository;
                              	use Doctrine\Common\Collections\ArrayCollection;
                              	use Doctrine\Common\Collections\Collection;
                              	use Doctrine\ORM\Mapping as ORM;
                                use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
                              	use Symfony\Component\Security\Core\User\UserInterface;
                              	use DateTimeImmutable;
                              	
                              	/**
                                * @ORM\Entity(repositoryClass=UserRepository::class)
                                * @ORM\Table(name="`user`")
                                * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
                                */
                               class User implements UserInterface
                              	{
                              		public const ROLE_USER = 'ROLE_USER';
                              		public const STATUS_NEW = '1';
                              		
                              		/**
                              		 * @ORM\Id
                              		 * @ORM\GeneratedValue
                              		 * @ORM\Column(type="integer")
                              		 */
                              		private $id;
                              		
                              		/**
                              		 * @ORM\Column(type="string", length=180, unique=true, nullable=false)
                              		 */
                              		private $email;
                              		
                              		/**
                              		 * @ORM\Column(type="string", length=180, nullable=false)
                              		 */
                              		private $firstname = '';
                              		
                              		/**
                              		 * @ORM\Column(type="string", length=180, nullable=false)
                              		 */
                              		private $lastname = '';
                              		
                              		/**
                              		 * @ORM\Column(type="json")
                              		 */
                              		private $roles = [];
                              		
                              		/**
                              		 * @ORM\Column(type="string", nullable=false)
                              		 */
                              		private $password;
                              		
                              		/**
                              		 * @ORM\Column(type="string", nullable=false)
                              		 */
                              		private $phone = '';
                              		
                              		/**
                              		 * @ORM\Column(type="integer", nullable=false)
                              		 */
                              		private $status;
                              		
                              		/**
                              		 * @var int
                              		 * @ORM\Column(type="integer")
                              		 */
                              		private $role = 1;
                              		
                              		/**
                              		 * @var dateTimeInterface
                              		 * @ORM\Column(type="datetime", nullable=false)
                              		 */
                              		private $updatedAt;
                              		
                              		/**
                              		 * @var dateTimeInterface
                              		 * @ORM\Column(type="datetime", nullable=false)
                              		 */
                              		private $createdAt;
                           
	                                /**
	                                 * @ORM\ManyToMany(targetEntity=Address::class, inversedBy="user")
	                                 * @ORM\JoinTable(name="user_to_address")
	                                 */
	                                private $addresses;
	      
	                                /**
	                                 * var int
	                                 * @ORM\Column(type="integer")
	                                 */
	                                private $isVerified = 0;
                              		
                              		public function __construct()
                              		{
                              			$this->updatedAt = new DateTimeImmutable();
                              			$this->createdAt = new DateTimeImmutable();
                                                $this->addresses = new ArrayCollection();
                              		}
                              		
                              		public function getId(): int
                              		{
                              			return $this->id;
                              		}
                              		
                              		public function getEmail(): string
                              		{
                              			if ($this->email) {
			                                return $this->email;
		                                } else {
                              				return '';
		                                }
                              			
                              		}
                              		
                              		public function setEmail(string $email): self
                              		{
                              			$this->email = $email;
                              			
                              			return $this;
                              		}
                              		
                              		/**
                              		 * A visual identifier that represents this user.
                              		 *
                              		 * @see UserInterface
                              		 */
                              		public function getUsername(): string
                              		{
                              			return (string)$this->email;
                              		}
                              		
                              		/**
                              		 * @see UserInterface
                              		 */
                              		public function getRoles(): array
                              		{
                              			$roles = $this->roles;
                              			// guarantee every user at least has ROLE_USER
                              			$roles[] = self::ROLE_USER;
                              			
                              			return array_unique($roles);
                              		}
                              		
                              		public function setRoles(array $roles): self
                              		{
                              			$this->roles = $roles;
                              			
                              			return $this;
                              		}
                              		
                              		/**
                              		 * @see UserInterface
                              		 */
                              		public function getPassword(): string
                              		{
                              			return $this->password;
                              		}
                              		
                              		public function setPassword(string $password): self
                              		{
                              			$this->password = $password;
                              			
                              			return $this;
                              		}
                              		
                              		/**
                              		 * Returning a salt is only needed, if you are not using a modern
                              		 * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
                              		 *
                              		 * @see UserInterface
                              		 */
                              		public function getSalt(): ?string
                              		{
                              			return null;
                              		}
                              		
                              		/**
                              		 * @see UserInterface
                              		 */
                              		public function eraseCredentials()
                              		{
                              			// If you store any temporary, sensitive data on the user, clear it here
                              			// $this->plainPassword = null;
                              		}
                              		
                              		/**
                              		 * @return string
                              		 */
                              		public function getPhone(): string
                              		{
                              			return $this->phone;
                              		}
                              		
                              		/**
                              		 * @param string $phone
                              		 */
                              		public function setPhone(string $phone): self
                              		{
                              			$this->phone = $phone;
                              			
                              			return $this;
                              		}
                              		
                              		/**
                              		 * @return int
                              		 */
                              		public function getStatus(): int
                              		{
                              			return $this->status;
                              		}
                              		
                              		/**
                              		 * @param int $status
                              		 */
                              		public function setStatus(int $status): self
                              		{
                              			$this->status = $status;
                              			
                              			return $this;
                              		}
                              		
                              		/**
                              		 * @return int
                              		 */
                              		public function getRole(): int
                              		{
                              			return $this->role;
                              		}
                              		
                              		/**
                              		 * @param int $role
                              		 */
                              		public function setRole(int $role): void
                              		{
                              			$this->role = $role;
                              		}
                              		
                              		/**
                              		 * @return dateTimeInterface
                              		 */
                              		public function getUpdatedAt()
                              		{
                              			return $this->updatedAt;
                              		}
                              		
                              		/**
                              		 * @param dateTimeInterface $updatedAt
                              		 */
                              		public function setUpdatedAt($updatedAt): self
                              		{
                              			$this->updatedAt = $updatedAt;
                              			
                              			return $this;
                              		}
                              		
                              		/**
                              		 * @return dateTimeInterface
                              		 */
                              		public function getCreatedAt()
                              		{
                              			return $this->createdAt;
                              		}
                              		
                              		/**
                              		 * @param dateTimeInterface $createdAt
                              		 */
                              		public function setCreatedAt($createdAt): self
                              		{
                              			$this->createdAt = $createdAt;
                              			
                              			return $this;
                              		}
                              		
                              		/**
                              		 * @return string
                              		 */
                              		public function getFirstname(): string
                              		{
                              			return $this->firstname;
                              		}
                              		
                              		/**
                              		 * @param string $firstname
                              		 */
                              		public function setFirstname(string $firstname): self
                              		{
                              			$this->firstname = $firstname;
                              			
                              			return $this;
                              		}
                              		
                              		/**
                              		 * @return string
                              		 */
                              		public function getLastname(): string
                              		{
                              			return $this->lastname;
                              		}
                              		
                              		/**
                              		 * @param string $lastname
                              		 */
                              		public function setLastname(string $lastname): self
                              		{
                              			$this->lastname = $lastname;
                              			
                              			return $this;
                              		}
                     
                                /**
                                 * @return Collection|Address[]
                                 */
                                public function getAddresses(): Collection
                                {
                                    return $this->addresses;
                                }
                  
                                public function addAddress(Address $address): self
                                {
                                    if (!$this->addresses->contains($address)) {
                                        $this->addresses[] = $address;
                                        $address->addUser($this);
                                    }
                  
                                    return $this;
                                }
               
                                public function removeAddress(Address $address): self
                                {
                                    if ($this->addresses->removeElement($address)) {
                                        $address->removeUser($this);
                                    }
               
                                    return $this;
                                }
   
                                public function isVerified(): bool
                                {
                                    return $this->isVerified;
                                }

                                public function setIsVerified(bool $isVerified): self
                                {
                                    $this->isVerified = $isVerified;

                                    return $this;
                                }
                              	}
