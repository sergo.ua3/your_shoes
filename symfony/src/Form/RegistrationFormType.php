<?php
	
	namespace App\Form;
	
	use App\Entity\User;
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
	use Symfony\Component\Form\Extension\Core\Type\EmailType;
	use Symfony\Component\Form\Extension\Core\Type\NumberType;
	use Symfony\Component\Form\Extension\Core\Type\PasswordType;
	use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
	use Symfony\Component\Form\Extension\Core\Type\SubmitType;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\IsTrue;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use function Sodium\add;
	
	class RegistrationFormType extends AbstractType
	{
		public function buildForm(FormBuilderInterface $builder, array $options): void
		{
			$builder
				->add('first_name', TextType::class, [
					'label' => false,
					'attr' => ['placeholder' => 'first_name' , 'style' => 'margin-top: 25px;']])
				->add('last_name', TextType::class, [
					'label' => false,
					'attr' => ['placeholder' => 'last_name' , 'style' => 'margin-top: 25px;']])
				->add('phone', TextType::class, [
					'label' => false,
					'attr' => ['placeholder' => 'phone' , 'style' => 'margin-top: 25px;']])
				->add('email', EmailType::class, [
					'label' => false,
					'attr' => ['placeholder' => 'email' , 'style' => 'margin-top: 25px;']])
				->add('plainPassword', RepeatedType::class, [
					// instead of being set onto the object directly,
					// this is read and encoded in the controller
					'mapped' => false,
					'label' => false,
					'type' => PasswordType::class,
					'required' => true,
					'first_options' => ['label' => false, 'attr' => ['placeholder' => 'password', 'style' => 'margin-top: 25px;']],
					'second_options' => ['label' => false, 'attr' => ['placeholder' => 'repeat_password' , 'style' => 'margin-top: 25px;']],
					'attr' => ['autocomplete' => 'new-password',
						'placeholder' => 'Password',
						'style' => 'margin-top: 20px;'],
					'constraints' => [
						new NotBlank([
							'message' => 'Please enter a password',
						]),
						new Length([
							'min' => 6,
							'minMessage' => 'Your password should be at least {{ limit }} characters',
							// max length allowed by Symfony for security reasons
							'max' => 4096,
						]),
					],
				])
				->add('submit', SubmitType::class, [
					'label' => 'reg.submit',
					'attr' => ['style' => 'margin-top: 25px;', 'class' => 'btn btn btn-gray-deep btn-hover-primary']]);
		}
		
		public function configureOptions(OptionsResolver $resolver): void
		{
			$resolver->setDefaults([
				'data_class' => User::class,
			]);
		}
	}
