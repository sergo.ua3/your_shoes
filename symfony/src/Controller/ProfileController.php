<?php
	
	namespace App\Controller;
	
	use App\Entity\User;
	use Doctrine\ORM\EntityManagerInterface;
	use http\Client\Request;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\Routing\Annotation\Route;
	use Symfony\Contracts\Translation\TranslatorInterface;
	
	class ProfileController extends AbstractController
	{
		/**
		 * @Route("/user/profile", name="_profile")
		 */
		
		public function profile(): Response
		{
			if (!$this->getUser()) {
				return $this->redirectToRoute('app_login');
			}
			return $this->render('profile/profile.html.twig', ['user' => $this->getUser()]);
		}
	}
